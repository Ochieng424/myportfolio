# Generated by Django 2.1.5 on 2019-02-11 15:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Works', '0004_auto_20190209_0732'),
    ]

    operations = [
        migrations.AlterField(
            model_name='work',
            name='image',
            field=models.ImageField(upload_to='Works/static/images/'),
        ),
    ]
