# Generated by Django 2.1.5 on 2019-02-18 12:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Contact', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contact',
            name='profile_link',
        ),
        migrations.RemoveField(
            model_name='contact',
            name='title',
        ),
        migrations.RemoveField(
            model_name='contact',
            name='username',
        ),
        migrations.AddField(
            model_name='contact',
            name='email',
            field=models.EmailField(default='me@gmail.com', max_length=70),
        ),
        migrations.AddField(
            model_name='contact',
            name='fullName',
            field=models.CharField(default='derrick', max_length=100),
        ),
        migrations.AddField(
            model_name='contact',
            name='message',
            field=models.TextField(default='ertyuiopretfyuiopertyui'),
        ),
        migrations.AddField(
            model_name='contact',
            name='subject',
            field=models.CharField(default='perfect', max_length=120),
        ),
    ]
